---
title: 'Snippets: Working within the tidyverse'
author: ''
date: '2018-06-25'
slug: snippets-working-within-the-tidyverse
categories:
  - R
tags:
  - tidy data
  - tidyverse
  - dplyr
summary: "Commands, tips, and tricks that are useful within the tidyverse of packages, especially when using piped operations."
output:
  blogdown::html_page:
    toc: true
thumbnailImagePosition: left
thumbnailImage: http://res.cloudinary.com/dn83gtg0l/image/upload/v1529964813/dots.jpg
---


<div id="TOC">
<ul>
<li><a href="#using-setnames-to-change-column-names">Using setNames to change column names</a></li>
<li><a href="#using-rename_at-to-rename-specific-columns">Using rename_at to rename specific columns</a></li>
<li><a href="#mutate-and-summarise-multiple-columns">Mutate and Summarise multiple columns</a></li>
<li><a href="#mutating-multiple-date-formats-within-the-same-column">Mutating multiple date formats within the same column</a></li>
<li><a href="#multiple-left_joins-using-dplyr">Multiple left_joins using dplyr</a></li>
<li><a href="#calculating-quantiles-the-tidy-way">Calculating quantiles the tidy way</a></li>
</ul>
</div>

<p>Snippet files are periodically updated with tips and tricks as I learn them. These commands are relevant for packages within the <code>tidyverse</code>.</p>
<div id="using-setnames-to-change-column-names" class="section level1">
<h1>Using setNames to change column names</h1>
<p>The <code>colnames</code> function can be used, as can <code>rename()</code>, but for ease of use while piping commands, the <code>setNames()</code> function is the easiest. You can either reference a vector of names or you can also reference a specific row within the data if that’s necessary. For example, in <a href="https://taylorgrant.netlify.com/2018/05/using-rvest-to-import-and-html-table/" target="_blank">this post</a> after scraping an html table the column names were in the second row. In that case, we can just reference the row by its location - <code>%&gt;% setNames(.[2,])</code></p>
<pre class="r"><code>names &lt;- c(&quot;name1&quot;, &quot;name2&quot;, &quot;name3&quot;, &quot;name4&quot;)

df &lt;- tibble(
  a = sample(letters, 4),
  b = sample(1:100, 4),
  c = sample(1:100, 4),
  d = sample(letters, 4)
)

df %&gt;% setNames(names)</code></pre>
<pre><code>## # A tibble: 4 x 4
##   name1 name2 name3 name4
##   &lt;chr&gt; &lt;int&gt; &lt;int&gt; &lt;chr&gt;
## 1 e        39    57 l    
## 2 u        13    16 k    
## 3 a        76    87 b    
## 4 r        73    40 c</code></pre>
</div>
<div id="using-rename_at-to-rename-specific-columns" class="section level1">
<h1>Using rename_at to rename specific columns</h1>
<p>Occasionally, you’ll only want to rename certain columns and the <code>rename_at</code> function offers this capability, in much the same way as <code>mutate_at</code> or <code>summarise_at</code>.</p>
<pre class="r"><code>df &lt;- tibble(
  a = sample(letters, 4),
  b = sample(1:100, 4),
  c = sample(1:100, 4),
  d = sample(letters, 4)
)

df &lt;- df %&gt;% 
  rename_at(vars(b,d), ~ paste0(&quot;pastedName_&quot;, .))
df</code></pre>
<pre><code>## # A tibble: 4 x 4
##   a     pastedName_b     c pastedName_d
##   &lt;chr&gt;        &lt;int&gt; &lt;int&gt; &lt;chr&gt;       
## 1 a               28    10 c           
## 2 x                3    64 k           
## 3 t               98    96 m           
## 4 q               48    32 r</code></pre>
</div>
<div id="mutate-and-summarise-multiple-columns" class="section level1">
<h1>Mutate and Summarise multiple columns</h1>
<p>This is well summarised <a href="https://dplyr.tidyverse.org/reference/summarise_all.html" target="_blank">here</a>, but a few examples are below. When using either <code>mutate_at()</code> or <code>summarise_at</code> it’s important to include the “vars(), funs()” format.</p>
<pre class="r"><code>mtcars %&gt;% 
  group_by(cyl) %&gt;%
  summarise_at(vars(disp, drat, mpg), funs(median, mean))</code></pre>
<pre><code>## # A tibble: 3 x 7
##     cyl disp_median drat_median mpg_median disp_mean drat_mean mpg_mean
##   &lt;dbl&gt;       &lt;dbl&gt;       &lt;dbl&gt;      &lt;dbl&gt;     &lt;dbl&gt;     &lt;dbl&gt;    &lt;dbl&gt;
## 1     4        108         4.08       26        105.      4.07     26.7
## 2     6        168.        3.9        19.7      183.      3.59     19.7
## 3     8        350.        3.12       15.2      353.      3.23     15.1</code></pre>
<p>The <a href="https://dplyr.tidyverse.org/reference/select_helpers.html" target="_blank">helper functions</a> for <code>select()</code> are also useful for selecting variables. Though note that when using helpers such as <code>contains()</code> that you can only include one string. For instance, this will work.</p>
<pre class="r"><code>mtcars %&gt;% 
  summarise_at(vars(contains(&quot;ar&quot;)), funs(mean))</code></pre>
<pre><code>##     gear   carb
## 1 3.6875 2.8125</code></pre>
<p>But this won’t</p>
<pre class="r"><code>mtcars %&gt;%
  summarise_at(vars(contains(&quot;ar|mp&quot;)), funs(mean))</code></pre>
<pre><code>## data frame with 0 columns and 0 rows</code></pre>
<p>If you want to match across multiple strings, the <code>matches()</code> function will do the trick.</p>
<pre class="r"><code>mtcars %&gt;%
  summarise_at(vars(matches(&quot;ar|mp&quot;)), funs(mean))</code></pre>
<pre><code>##        mpg   gear   carb
## 1 20.09062 3.6875 2.8125</code></pre>
</div>
<div id="mutating-multiple-date-formats-within-the-same-column" class="section level1">
<h1>Mutating multiple date formats within the same column</h1>
<p>I was recently dealing with several large datasets that contain multiple date formats. I had been trying to set the dates as <code>as.POSIXct</code> formats, and it wasn’t until trying to use the <code>difftime</code> function that I realized several years were in two digit, rather than four digit format. The problem is that when specifying the date format, a two digit year pads itself with two extra zeros as seen below in the try_date column.</p>
<pre class="r"><code>df &lt;- tibble(
  dates = c(&quot;30/05/2017 07:20&quot;, &quot;19/6/17 13:47&quot;)
)

df %&gt;% mutate(try_date = as.POSIXct(dates, tz = &quot;&quot;, format = &quot;%d/%m/%Y %H:%M&quot;))</code></pre>
<pre><code>## # A tibble: 2 x 2
##   dates            try_date           
##   &lt;chr&gt;            &lt;dttm&gt;             
## 1 30/05/2017 07:20 2017-05-30 07:20:00
## 2 19/6/17 13:47    0017-06-19 13:47:00</code></pre>
<p>To get around this, we can use the <code>lubridate</code> package to solve our issues. In this case, I’m splitting out my dates and times before gluing them back together. the <code>dmy()</code> function automatically converts the <code>%y</code> into <code>%Y</code>.</p>
<pre class="r"><code>df &lt;- tibble(
  dates = c(&quot;30/05/2017 07:20&quot;, &quot;19/6/17 13:47&quot;)
)

df %&gt;% 
  mutate(working_date = lubridate::dmy(gsub(&quot;\\ .*&quot;, &quot;&quot;, dates)),
         hour = gsub(&quot;.*\\ &quot;, &quot;&quot;, dates),
         final_date = as.POSIXct(glue(&quot;{working_date} {hour}&quot;), tz=&quot;&quot;, format = &quot;%Y-%m-%d %H:%M&quot;))</code></pre>
<pre><code>## # A tibble: 2 x 4
##   dates            working_date hour  final_date         
##   &lt;chr&gt;            &lt;date&gt;       &lt;chr&gt; &lt;dttm&gt;             
## 1 30/05/2017 07:20 2017-05-30   07:20 2017-05-30 07:20:00
## 2 19/6/17 13:47    2017-06-19   13:47 2017-06-19 13:47:00</code></pre>
</div>
<div id="multiple-left_joins-using-dplyr" class="section level1">
<h1>Multiple left_joins using dplyr</h1>
<p>It’s always possible to use multiple <code>left_join</code> functions, but the easiest way to do merge multiple data sets together may be to put everything into a list and then use the <code>Reduce</code> function. I had used <code>map</code> to work over a lot of data, so everything was in a list. I then took the data I wanted to use as my base and concatenated it to the list.</p>
<pre><code>tmp &lt;- c(list(df), original_list)</code></pre>
<p>Then, using dplyr commands was able to join all of the data. In this case, my original list had 30 separate dataframes.</p>
<pre><code>mass_df &lt;- tmp %&gt;% Reduce(function(df1, df2), left_join(df1, df2), .)</code></pre>
<p>The <code>left_join</code> command can be further defined to specify what we’re joining by, or to select only specific columns that will be joined.</p>
<pre><code># using &quot;matches&quot; to pull out specific variables
mass_df &lt;- tmp %&gt;% Reduce(function(df1, df2), left_join(df1, select(df2, matches(&quot;avar|bvar&quot;))), .)

# specifying what the join is by
mass_df &lt;- tmp %&gt;% Reduce(function(df1, df2), left_join(df1, df2, by = &quot;index&quot;), .)</code></pre>
</div>
<div id="calculating-quantiles-the-tidy-way" class="section level1">
<h1>Calculating quantiles the tidy way</h1>
<p>This is straight from Stack Overflow, but comes in handy. Note that “probs” can be added to the <code>quantile</code> command depending on specific needs.</p>
<pre class="r"><code>mtcars %&gt;%
  nest(-cyl) %&gt;% 
  mutate(quantiles = map(data, ~ quantile(.$mpg))) %&gt;%
  unnest(map(quantiles, broom::tidy))</code></pre>
<pre><code>##    cyl names     x
## 1    6    0% 17.80
## 2    6   25% 18.65
## 3    6   50% 19.70
## 4    6   75% 21.00
## 5    6  100% 21.40
## 6    4    0% 21.40
## 7    4   25% 22.80
## 8    4   50% 26.00
## 9    4   75% 30.40
## 10   4  100% 33.90
## 11   8    0% 10.40
## 12   8   25% 14.40
## 13   8   50% 15.20
## 14   8   75% 16.25
## 15   8  100% 19.20</code></pre>
</div>
