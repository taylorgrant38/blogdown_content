---
title: 'Snippets: tidy data'
author: ''
date: '2018-02-25'
slug: snippets-tidyr
categories:
  - R 
tags:
  - tidyr
summary: "These commands are relevant for tidying dataframes - to rearrange data and change the structure of the data for easier use."
output:
  blogdown::html_page:
    toc: true
thumbnailImagePosition: left
thumbnailImage: http://res.cloudinary.com/dn83gtg0l/image/upload/v1529964813/dots.jpg
---


<div id="TOC">
<ul>
<li><a href="#spread">spread()</a></li>
<li><a href="#gather">gather()</a></li>
<li><a href="#separate">separate()</a></li>
<li><a href="#unite">unite()</a></li>
</ul>
</div>

<p>Snippet files are periodically updated with how-to’s for data wrangling. These commands are relevant for tidying dataframes - to rearrange data and change the structure of the data for easier use.</p>
<div id="spread" class="section level1">
<h1>spread()</h1>
<p>Spread is based on the key, value pairing – the key becomes the names of the new columns and the values are row values within each new column.</p>
<pre class="r"><code>pacman::p_load(tidyverse, blogdown, glue, here)
dat &lt;- read_csv(&quot;https://raw.githubusercontent.com/garrettgman/DSR/master/data-raw/table2.csv&quot;)

dat</code></pre>
<pre><code>## # A tibble: 12 x 4
##    country      year key             value
##    &lt;chr&gt;       &lt;int&gt; &lt;chr&gt;           &lt;int&gt;
##  1 Afghanistan  1999 cases             745
##  2 Afghanistan  1999 population   19987071
##  3 Afghanistan  2000 cases            2666
##  4 Afghanistan  2000 population   20595360
##  5 Brazil       1999 cases           37737
##  6 Brazil       1999 population  172006362
##  7 Brazil       2000 cases           80488
##  8 Brazil       2000 population  174504898
##  9 China        1999 cases          212258
## 10 China        1999 population 1272915272
## 11 China        2000 cases          213766
## 12 China        2000 population 1280428583</code></pre>
<pre class="r"><code>dat %&gt;% spread(key, value)</code></pre>
<pre><code>## # A tibble: 6 x 4
##   country      year  cases population
##   &lt;chr&gt;       &lt;int&gt;  &lt;int&gt;      &lt;int&gt;
## 1 Afghanistan  1999    745   19987071
## 2 Afghanistan  2000   2666   20595360
## 3 Brazil       1999  37737  172006362
## 4 Brazil       2000  80488  174504898
## 5 China        1999 212258 1272915272
## 6 China        2000 213766 1280428583</code></pre>
</div>
<div id="gather" class="section level1">
<h1>gather()</h1>
<p><code>gather</code> is going to work in the opposite direction. It gathers the column names, puts them in a single column, and then matches the value to each row. Note that you need to specify the columns you want to bring into the gathering.</p>
<pre class="r"><code>library(tidyverse)
dat &lt;- read_csv(&quot;https://raw.githubusercontent.com/garrettgman/DSR/master/data-raw/table4.csv&quot;)

dat</code></pre>
<pre><code>## # A tibble: 3 x 3
##   country     `1999` `2000`
##   &lt;chr&gt;        &lt;int&gt;  &lt;int&gt;
## 1 Afghanistan    745   2666
## 2 Brazil       37737  80488
## 3 China       212258 213766</code></pre>
<pre class="r"><code>dat %&gt;% gather(&quot;year&quot;, &quot;cases&quot;, 2:3)</code></pre>
<pre><code>## # A tibble: 6 x 3
##   country     year   cases
##   &lt;chr&gt;       &lt;chr&gt;  &lt;int&gt;
## 1 Afghanistan 1999     745
## 2 Brazil      1999   37737
## 3 China       1999  212258
## 4 Afghanistan 2000    2666
## 5 Brazil      2000   80488
## 6 China       2000  213766</code></pre>
<p><code>gather</code> works similar to the dplyr function <code>select</code> in that you can also use the -varname to specify which variable isn’t included.</p>
<pre class="r"><code>library(tidyverse)
dat &lt;- read_csv(&quot;https://raw.githubusercontent.com/garrettgman/DSR/master/data-raw/table4.csv&quot;)

dat %&gt;% gather(year, cases, -country)</code></pre>
<pre><code>## # A tibble: 6 x 3
##   country     year   cases
##   &lt;chr&gt;       &lt;chr&gt;  &lt;int&gt;
## 1 Afghanistan 1999     745
## 2 Brazil      1999   37737
## 3 China       1999  212258
## 4 Afghanistan 2000    2666
## 5 Brazil      2000   80488
## 6 China       2000  213766</code></pre>
</div>
<div id="separate" class="section level1">
<h1>separate()</h1>
<p><code>separate</code> works when a dataframe includes compound variables - for instance, age and gender demographics. <code>separate</code> works in one of two ways - first, you can pass it a regular expression to split on, but if you choose not to give it, it will default to non-alphanumeric character to split on, as below.</p>
<pre class="r"><code>df &lt;- data.frame(x = c(NA, &quot;m.14&quot;, &quot;f.20&quot;, &quot;f.45&quot;))
df %&gt;% separate(x, c(&quot;Gender&quot;, &quot;Age&quot;))</code></pre>
<pre><code>##   Gender  Age
## 1   &lt;NA&gt; &lt;NA&gt;
## 2      m   14
## 3      f   20
## 4      f   45</code></pre>
<p>Second, you can tell it how many characters in to split on. If we change our example somewhat, we can see where this might be useful.</p>
<pre class="r"><code>df &lt;- data.frame(x = c(NA, &quot;m14&quot;, &quot;f20&quot;, &quot;f45&quot;))
df %&gt;% separate(x, c(&quot;Gender&quot;, &quot;Age&quot;) , 1)</code></pre>
<pre><code>##   Gender  Age
## 1   &lt;NA&gt; &lt;NA&gt;
## 2      m   14
## 3      f   20
## 4      f   45</code></pre>
</div>
<div id="unite" class="section level1">
<h1>unite()</h1>
<p>The <code>unite</code> function is helpful in that it pulls disparate columns together into one. To see how it works, we’ll make some fake data, and then use <code>unite</code> to pull the hour, min, and second into a single variable.</p>
<pre class="r"><code>date &lt;- as.Date(&#39;2018-01-01&#39;) + 0:14
hour &lt;- sample(1:24, 15)
min &lt;- sample(1:60, 15)
second &lt;- sample(1:60, 15)
event &lt;- sample(letters, 15)
data &lt;- data.frame(date, hour, min, second, event)
data</code></pre>
<pre><code>##          date hour min second event
## 1  2018-01-01   12  28     58     e
## 2  2018-01-02    6  16     13     x
## 3  2018-01-03    4  54     45     t
## 4  2018-01-04   11   5     28     o
## 5  2018-01-05   17   7     47     m
## 6  2018-01-06   14  22     27     d
## 7  2018-01-07   21  34     32     k
## 8  2018-01-08    5  50     18     l
## 9  2018-01-09   19  48      7     p
## 10 2018-01-10    3  49     42     i
## 11 2018-01-11   24  37     29     n
## 12 2018-01-12   15  40      1     s
## 13 2018-01-13    2  42     12     g
## 14 2018-01-14    8  11     26     z
## 15 2018-01-15   13  47     51     r</code></pre>
<pre class="r"><code>data %&gt;% unite(datetime, hour, min, second, sep = &quot;:&quot;)</code></pre>
<pre><code>##          date datetime event
## 1  2018-01-01 12:28:58     e
## 2  2018-01-02  6:16:13     x
## 3  2018-01-03  4:54:45     t
## 4  2018-01-04  11:5:28     o
## 5  2018-01-05  17:7:47     m
## 6  2018-01-06 14:22:27     d
## 7  2018-01-07 21:34:32     k
## 8  2018-01-08  5:50:18     l
## 9  2018-01-09  19:48:7     p
## 10 2018-01-10  3:49:42     i
## 11 2018-01-11 24:37:29     n
## 12 2018-01-12  15:40:1     s
## 13 2018-01-13  2:42:12     g
## 14 2018-01-14  8:11:26     z
## 15 2018-01-15 13:47:51     r</code></pre>
<pre class="r"><code># the default separator is &quot;_&quot;, so if that doesn&#39;t work, change it as necessary</code></pre>
<p>The <code>unite</code> function is also helpful when you need to spread multiple columns in a dataframe.</p>
<pre class="r"><code>df &lt;- data.frame(quarter=rep(1:4,2),
                 section=rep(c(&quot;Sec1&quot;, &quot;Sec2&quot;), each=4),
                 QA=c(.9, .7, .6, .8, .6, .9, 1.0, .6),
                 QB=c(.6, .7, .8, .5, .6, .7, .5, .9))

df</code></pre>
<pre><code>##   quarter section  QA  QB
## 1       1    Sec1 0.9 0.6
## 2       2    Sec1 0.7 0.7
## 3       3    Sec1 0.6 0.8
## 4       4    Sec1 0.8 0.5
## 5       1    Sec2 0.6 0.6
## 6       2    Sec2 0.9 0.7
## 7       3    Sec2 1.0 0.5
## 8       4    Sec2 0.6 0.9</code></pre>
<p>What if we want to spread this so that each column relates to a section and a class average on two quizzes each quarter? We would use a lot of what the <code>tidyr</code> package has to offer - gather, unite, and then spread.</p>
<pre class="r"><code>df &lt;- data.frame(quarter=rep(1:4,2),
                 section=rep(c(&quot;Sec1&quot;, &quot;Sec2&quot;), each=4),
                 QA=c(.9, .7, .6, .8, .6, .9, 1.0, .6),
                 QB=c(.6, .7, .8, .5, .6, .7, .5, .9))


df &lt;- df %&gt;% 
  gather(variable, value, -(quarter:section)) %&gt;%
  unite(tmp, section, variable) %&gt;%
  spread(tmp, value)

df</code></pre>
<pre><code>##   quarter Sec1_QA Sec1_QB Sec2_QA Sec2_QB
## 1       1     0.9     0.6     0.6     0.6
## 2       2     0.7     0.7     0.9     0.7
## 3       3     0.6     0.8     1.0     0.5
## 4       4     0.8     0.5     0.6     0.9</code></pre>
</div>
